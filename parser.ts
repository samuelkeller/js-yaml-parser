import YAML from 'yaml'
import fs from 'fs'
console.log("[Starting parser]")

const FILEPATH = process.argv[2];
if (!FILEPATH) {
    throw new Error("Path manquant")
}

console.log("Parsing file : " + FILEPATH);


const file = fs.readFileSync(FILEPATH, 'utf8')
YAML.parse(file, {prettyErrors: true})

console.log("[Ending parser]")